﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject[] objectsToSpawn;

    public float timeBetweenSpawn;
    private float timer;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if (timer > timeBetweenSpawn)
        {
            float number = Random.Range(-4f, 4f);
            Debug.Log(number);

            int whatToSpawn = Random.Range(0, 3);

            Instantiate(objectsToSpawn[whatToSpawn], transform.position + (Vector3.up * number), Quaternion.identity);

            timer = 0f;
        }
    }
}
