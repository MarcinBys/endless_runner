﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    private Rigidbody2D rb;

    private static bool win;
    private static bool lose;
    
    public Text endText;
    public Text scoreText;

    private static int score;

    void Start()
    {
        win = false;
        lose = false;

        endText.text = "";
        scoreText.text = "Score: 0";

        rb = GetComponent<Rigidbody2D>();
    }



    void Update()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            if (!win && !lose)
            {
                UpdateScore();
            }
            else if (win)
            {
                endText.text = "You WIN";
            }
            else if (lose)
            {
                endText.text = "You LOSE";
                gameObject.GetComponent<BoxCollider2D>().enabled = false;
                gameObject.GetComponent<SpriteRenderer>().enabled = false;
                
                if (Input.touchCount > 1)
                {
                    SceneManager.LoadScene(SceneManager.GetActiveScene().name);
                }
            }
        }
    }



    public static void Lose()
    {
        lose = true;
    }



    public static void Win()
    {
        win = true;
    }


    public static void AddScore(int points)
    {
        score += points;
    }


    private void UpdateScore()
    {
        scoreText.text = "Score: " + score.ToString();
    }
}