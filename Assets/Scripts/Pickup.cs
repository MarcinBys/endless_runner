﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour
{
    public float speed;

    private float lifeTime;
    private float time;

    private void Start()
    {
        lifeTime = 4;
    }

    private void Update()
    {
        time += Time.deltaTime;

        transform.Translate(Vector2.left * speed);
        
        if (time > lifeTime)
        {
            Destroy(gameObject);
        }
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (gameObject.CompareTag("Score"))
            {
                PlayerController.AddScore(10);
                Destroy(gameObject);
            }
            else if (gameObject.CompareTag("Killer"))
            {
                PlayerController.Lose();
            }
        }
    }
}