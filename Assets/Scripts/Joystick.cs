﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Joystick : MonoBehaviour
{
    //  Jostick controll parametrs
    public Transform player;
    public float speed = 2.0f;
    private bool touchStart = false;
    private Vector2 startingPoint;
    private Vector2 currentPoint;

    // Joystick visual
    
    public Transform circle;
    public Transform outerCircle;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        JControl();
        JVisual();
    }

    private void FixedUpdate()
    {
        JControlFixed();
    }
    // Control methods
    private void JControl()
    {
        if (Input.touchCount == 1)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                startingPoint = Camera.main.ScreenToWorldPoint(new Vector3(touch.position.x, touch.position.y, Camera.main.transform.position.z));
            }
            if (touch.phase == TouchPhase.Moved)
            {
                currentPoint = Camera.main.ScreenToWorldPoint(new Vector3(touch.position.x, touch.position.y, Camera.main.transform.position.z));
                touchStart = true;
            }
        }
        else
        {
            touchStart = false;
        }
    }

    private void JControlFixed()
    {
        if (touchStart)
        {
            Vector2 direction = Vector2.ClampMagnitude(currentPoint - startingPoint, 1.0f);
            Vector3 dirY = new Vector3(0,direction.y,0);
            player.Translate(dirY * speed * Time.deltaTime);
            circle.transform.position = new Vector2(startingPoint.x + direction.x, startingPoint.y + direction.y);
        }
    }
    
    // Visual methods methods
    private void JVisual()
    {
        if (Input.touchCount == 1)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                startingPoint = Camera.main.ScreenToWorldPoint(new Vector3(touch.position.x, touch.position.y, Camera.main.transform.position.z));
                circle.transform.position = startingPoint;
                outerCircle.transform.position = startingPoint;
                circle.GetComponent<SpriteRenderer>().enabled = true;
                outerCircle.GetComponent<SpriteRenderer>().enabled = true;
            }
            
        }
        else
        {
            circle.GetComponent<SpriteRenderer>().enabled = false;
            outerCircle.GetComponent<SpriteRenderer>().enabled = false;
            touchStart = false;
        }
    }
    
  

}
